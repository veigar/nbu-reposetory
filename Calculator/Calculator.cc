#include <iostream>
using namespace std;

int main() {
    char operation;
    double operand_one, operand_two;
    cout << "Please enter operation -, +, x, /: ";
    cin >> operation;
    cout << "Please enter the first operand:" << endl;
    cin >> operand_one;
    cout << "Please enter the second operand:" << endl;
    cin >> operand_two;
    switch(operation){
        case '+' : cout << operand_one << " + " << operand_two << " = " << operand_one + operand_two; break;
        case '-': cout << operand_one << " - " << operand_two << " = " << operand_one - operand_two; break;
        case 'x': cout << operand_one << " x " << operand_two << " = " << operand_one * operand_two; break;
        case '/': cout << operand_one << " / " << operand_two << " = " << operand_one / operand_two; break;
    }

    return 0;
}
