#include <iostream>
using namespace std;

#define POWER 2

int main() {
    int number;
    do {
        cout << "Please enter a number to check: " << endl;
        cin >> number;
    } while ( number <= 0 || number >= 100);

    int temp = number, power = 0;

    while( temp%POWER == 0 ) {
        temp/=POWER;
        power++;
    }
    ( temp == 1 ) ? cout << "The number " << number << " is " << power << " power of 2" << endl : cout << "The number " << number << " is not a power of 2";


    return 0;
}
