
#include <iostream>
#include <string>
#include <math.h>
#include <sstream>
#include <iomanip>

using namespace std;
const int NUMBER_OF_GRADES = 3;

int main() {

    string first_name, last_name, faculty_number;

    cout << "Please enter your names" << endl;
    cin >> first_name >> last_name;

    cout << "Please enter your faculty number" << endl;
    cin >> faculty_number;

    double grades[NUMBER_OF_GRADES];
    double median;

    cout << "Please enter the students grades" << endl;
    int i = 0;
    do {
        cin >> grades[i];
        if( grades[i] < 2 || grades[i] > 6 ) {
            cout << "The grade " << grades[i] << " is incorrect please enter a valid grade" << endl;
        } else {
            median += grades[i];
            cout << median << " " << endl;
        }


    } while( sizeof(grades) < 3 );

    median = median / NUMBER_OF_GRADES ;


    cout << setprecision(2) << faculty_number << " :" << median << endl;
    return 0;
}
