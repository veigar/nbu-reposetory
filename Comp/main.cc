
#include <cmath>
#include <iostream>
using namespace std;
class Comp{
	double x,y;
public:

	Comp(){x=y=0;}

	Comp( double x0, double y0=0){
		x=x0;y=y0;
	}

	double getX() const{ return x; }
	double getY() const{ return y; }

	void setX( double x){ this->x=x; }
	void setY( double y){ this->y=y; }


	bool operator==( const Comp & c) const{ return x==c.x && y==c.y;}
	bool operator!=( const Comp & c) const{ return x!=c.x || y!=c.y;}

	Comp operator+( const Comp &c) const{ return Comp(x+c.x,y+c.y);}
	Comp operator*( const Comp &c) const{ return Comp(x*c.x,y*c.y);}
	Comp operator-( const Comp &c) const{ return Comp(x-c.x,y-c.y);}
	Comp operator/( const Comp &c) const{ return Comp(x/c.x,y/c.y);}

	Comp operator+( double d) const{ return Comp(x+d,y+d);}
	Comp operator*( double d) const{ return Comp(x*d,y*d);}
	Comp operator-( double d) const{ return Comp(x-d,y-d);}
	Comp operator/( double d) const{ return Comp(x/d,y/d);}

	Comp operator-(){return Comp(-x,-y);}
	Comp operator~()const{return Comp(x,-y);}
	Comp operator++(){++x;y++;return Comp(x,y);}
	Comp operator++(int){return Comp(x++,y++);}
	Comp sqr()const{return *this * *this;}

	ostream & ins(ostream& os)const{return os<<'('<<x<<','<<y<<')';}
};
    //�������� ������������� ��������:
	//�������� � ������� �����
	ostream& operator<<(ostream& o,const Comp & c){return c.ins(o);}

	Comp operator+( double d, const Comp &c) { return c+d;}
	Comp operator*( double d, const Comp &c) { return c*d;}
	Comp operator-( double d, const Comp &c) { return Comp(d,d)-c;}
	Comp operator/( double d, const Comp &c) { return Comp(d,d)/c;}

int main(){
	Comp a(1,1), b(2, 2);
	cout << "a.x = " << a.getX() << " a.y = " << a.getY() << endl;
	a.setX(2);
	a.setY(2);
	cout << "a = " << a << endl;
	cout << "b = " << b << endl;
	cout << "a == b ? " << ( a == b ) << endl;
	cout << "a != b ? " << ( a != b )  << endl;
	cout << "a = " << a << ", b = " << b << ", a+b = " << a+b << endl;
	cout << "a = " << a << ", b = " << b <<", a-b = " << a-b << endl;
    cout << "a = " << a << ", b = " << b << ", a*b = " << a*b << endl;
    cout << "a = " << a << ", b = " << b << ", a/b = " << a/b << endl;
	cout << "a = " << a << ", a + 1 = " << a+1 << endl;
	cout << "a = " << a << ", a - 1 = " << a-1 << endl;
	cout << "a = " << a << ", a * 1 = " << a*1 << endl;
	cout << "a = " << a << ", a / 1 = " << a/1 << endl;
	cout << "a = " << a << ", -a = " << -a << endl;
	cout << "a = " << a << ", ~a = " << ~a << endl;
	cout << "a = " << a;
	cout << ", a++ = " << (a++) << ", a = " << at << endl;
	cout << "a = " << a;
	cout << ", ++a = " << (++a) << endl;
	cout << "a = " << a << ", a.sqr() = " << a.sqr() << endl;
	cout << "a = " << a << ", 1 + a = " << 1 + a << endl;
 	cout << "a = " << a << ", 1 - a = " << 1 - a << endl;
 	cout << "a = " << a << ", 1 * a = " << 1 * a << endl;
 	cout << "a = " << a << ", 1 / a = " << 1 / a << endl;

	return 0;
}
