#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int width = 3;
    int height = 4;
    int pointerArray[3][4] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };

    for( int i = 0; i < width; i++ ) {
            for( int k = 0; k < height; k++ ) {
                cout << setw(5) << *((int *)pointerArray + height*i + k ) ;
            }
        cout << endl;
    }

    for ( int i=0; i < height; i++) {
        int temp = *((int *)pointerArray + height*0 + i );
        *((int *)pointerArray + height*0 + i ) = *((int *)pointerArray + height*2 + i );
        *((int *)pointerArray + height*2 + i ) = temp;
    }

    cout << endl;

    for( int i = 0; i < width; i++ ) {
            for( int k = 0; k < height; k++ ) {
                cout << setw(5) << *((int *)pointerArray + height*i + k ) ;
            }
        cout << endl;
    }

    cout << endl;

    for ( int i=0; i < height; i++) {
        int temp = *((int *)pointerArray + i*height + 1 );
        *((int *)pointerArray + i*height + 1 ) = *((int *)pointerArray + i*height + 3 ) ;
        *((int *)pointerArray + i*height + 3 ) = temp; // 1
    }



    for( int i = 0; i < width; i++ ) {
            for( int k = 0; k < height; k++ ) {
                cout << setw(5) << *((int *)pointerArray + height*i + k ) ;
            }
        cout << endl;
    }


    return 0;
}
