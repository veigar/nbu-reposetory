#include <iostream>
using namespace std;

int findMedian( int n ){
    int sum = 0, length = 0;
    while( n > 0 ) {
        sum += n%10;
		n /= 10;
		length++;
    }
    return sum/length;
}

int main() {
    int n;
    cout << "Enter a number to evaluate: ";
    cin >>  n;
    cout << findMedian(n);

    return 0;
}
