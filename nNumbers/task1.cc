#include <iostream>
using namespace std;

void printArray( double * arr, int n ) {
    for ( int i = 0; i < n; i++ ) {
        cout << arr[i];
        if( i != n -1 ) {
            cout << " , ";
        }
    }
    cout << endl;
}

int main() {
    int n;
    cout << "Please enter the amount of numbers you wish to read." << endl;
    cin >> n;

    double *arr = new double(n);
    for( int i = 0; i < n; i++ ) {
        cout << "arr[" << i << "] = ";
        cin >> arr[i];
    }
    double mMax = arr[0];
    for( int i = 0; i < n; i++ ) {
        if( arr[i] > mMax ) mMax = arr[i];
    }

    printArray( arr, n );
    cout << "The biggest number is: " << mMax << endl;

}
