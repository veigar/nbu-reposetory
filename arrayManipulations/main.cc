#include <iostream>
#include <ctime>
#include <cmath>
#include <cstdlib>
using namespace std;


void supplyArray(int *arr, int length) {
    srand(time(NULL));
    for( int i = 0; i < length; i++) {
        arr[i] = (rand() % 145) - 4;
        cout << arr[i];
        if( i != length -1 ) {
            cout << " , ";
        }
    }
    cout << endl;
}

void printArray( int * arr, int n ) {
    for ( int i = 0; i < n; i++ ) {
        cout << arr[i];
        if( i != n -1 ) {
            cout << " , ";
        }
    }
    cout << endl;
}
int findClosestToRandom( int * arr, int length ) {
    srand(time(NULL));
    int rnd = ( rand() % 99 ) + 1;
    int closest = arr[0];
    for( int i = 1; i < length; i++ ) {
        if( abs(rnd-closest) >= abs(rnd-arr[i])) closest = arr[i];
    }
    cout << "The number in the array, closest to " << rnd << " is ";
    return closest;
}

void mutateArray(int * arr, int length) {
    for( int i = 0; i < length; i++ ) {
        if( i%2 == 1 ){
            if( i+1 < length ) {
                arr[i] = arr[i-1] + arr[i+1];
            }
        }
    }
}

int main() {

    const int length = 10;
    int * arr = new int(length);
    supplyArray( arr, length );
    printArray(arr, length);
    cout << findClosestToRandom(arr, length) << endl;
    mutateArray(arr, length);
    printArray(arr,length);

    return 0;
}
