/***
FN:F73826
PID:2
GID:2
*/

#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <stdlib.h>

using namespace std;

void initialize_vector( vector<double> &v, int n ) {
    double value;
    for(int i = 0; i < n; i++ ) {
        cin >> value;
        v.push_back(value);
    }
}

void print_vector( vector<double> &v ) {
    for( int i=0; i < v.size(); i++ ) {
        cout << v[i] << " ";
    }
    cout << endl;
}

int main()
{
	int N, M;

	cin >> N;
	vector<double> v;
	vector<double> v2;
	initialize_vector(v,N);
    cin >> M;
    int * indexes  = new int[M];
    for( int i =0; i < M; i++ ) {
        cin >> indexes [i];
    }
    for( int i = 0; i < M; i++ ) {
        v2.push_back(v[indexes [i]]);
    }
    print_vector(v2);

	return 0;
}
