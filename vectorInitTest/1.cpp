#include <iostream>
#include <string>
#include <vector>

using namespace std;

double calculate_sum( vector<double> &vct_one, vector<double> &vct_two, int n ) {
    double hsum = 0;
    for( int i = 0; i < n; i++ ) {
        hsum += vct_one[i]*vct_two[i];
    }
    return hsum;
}



void init( vector<double> &vct, int n ) {
    double val;
    for(int i = 0; i < n; i++ ) {
        cin >> val;
        vct.push_back(val);
    }
}

int main()
{
        int N;
        cin >> N;
        vector<double> vct_one;
        vector<double> vct_two;
        init(vct_one,N);
        init(vct_two,N);
        cout << calculate_sum(vct_one, vct_two, N) << endl;
        return 0;
}
