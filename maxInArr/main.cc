#include <iostream>
using namespace std;

int arrMax( int * arr, int length ) {
    int max = arr[0];
    for ( int i = 1; i < length; i ++ ) {
        if( arr[i] > max ) max = arr[i];

    }
    return max;
}

int main() {

    const int length = 10;
    int arr[length] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    cout << "Your array is { ";
    for( int i = 0; i < length; i++ ) {
        cout << arr[i];
        if( i != length - 1 ) {
            cout << " , ";
        }
    }
    cout << " }" << endl;
    cout << "The max value in it is: " << arrMax(arr, length) << endl;
    return 0;

}
