#include <iostream>
#include <string>
using namespace std;

struct Planet{
     string name;
     long distance;
     double diameter;
     double mass;
};

void add_panet(Planet & p) {
    cout << "Enter planet\'s name." << endl;
    cin >> p.name;
    cout << "Enter planet\'s distance from the sun in milion kilometers." << endl;
    cin >> p.distance;
    cout << "Enter planet\'s diameter." << endl;
    cin >> p.diameter;
    cout << "Enter planet\'s mass." << endl;
    cin >> p.mass;
}
void show_planet_details( Planet & p ) {
    cout << "Name: " << p.name << endl;
    cout << "Distance: " << p.distance << endl;
    cout << "Diameter: " << p.diameter << endl;
    cout << "Mass: " << p.mass << endl;
}
void time_from_sun( Planet * planets, int n ) {
    const double speed_of_light = 299792458;
    for( int i=0; i < n; i++ ) {
        cout << "Planet " << i << " distance from the sun is: " << ( planets[i].distance * 1000000 ) / speed_of_light << " seconds" << endl;
    }
}

void find_heaviest_planet( Planet * planets, int n ) {
    Planet heaviest_planet = planets[0];
    for( int i=1; i < n; i++ ) {
       if( heaviest_planet.mass < planets[i].mass ) heaviest_planet = planets[i];
    }
    show_planet_details(heaviest_planet);
}
void find_smallets_planet( Planet * planets, int n ) {
    Planet smallets_planet = planets[0];
    for( int i=1; i < n; i++ ) {
       if( smallets_planet.diameter > planets[i].diameter ) smallets_planet = planets[i];
    }
    show_planet_details(smallets_planet);
}
void find_farest_planet( Planet * planets, int n ) {
    Planet farest_planet = planets[0];
    for( int i=1; i < n; i++ ) {
       if( farest_planet.diameter < planets[i].diameter ) farest_planet = planets[i];
    }
    show_planet_details(farest_planet);
}
void print_planets( Planet * planets, int n ) {
    for( int i=0; i < n; i++ ) {
        show_planet_details(planets[i]);
    }
}
void sort_planets_by_distance( Planet * planets, int n ) {
    Planet temp;

    for( int i = 0; i < ( n - 1 ); i++ ) {
        for( int j = 0; j < n - i - 1; j++ ) {
            if( planets[j].distance > planets[j+1].distance ) {
                temp = planets[j];
                planets[j] = planets[j+1];
                planets[j+1] = temp;
            }
        }
    }
    print_planets(planets, n);
}

int main() {
    Planet * planets;
    int length, mode;
    do{
        cout << "Enter mode: \n" <<
            "1 - enter planet details\n" <<
            "2 - show distance from Sun\n" <<
            "3 - show details for the heaviest planet\n" <<
            "4 - show details for the planet with smallest diameter\n" <<
            "5 - show details for the farest from the Sun\n" <<
            "6 - show details for all planets\n" <<
            "7 - show planets from the closest to the farest\n" <<
            "0 - exit program" << endl;
        cin >> mode;
        switch( mode ) {
            case 1: {
                cout << "Enter the number of planes you wish to input." << endl;
                cin >> length;
                planets = new Planet[length];
                for ( int i = 0; i < length; i++ ) {
                    add_panet(planets[i]);
                }
                break;
            }
            case 2: {
                time_from_sun(planets, length);
                break;
            }
            case 3: {
                find_heaviest_planet(planets, length);
                break;
            }
            case 4: {
                find_smallets_planet(planets, length);
                break;
            }
            case 5: {
                find_farest_planet(planets, length);
                break;
            }
            case 6:
                print_planets(planets, length);
                break;
            case 7: {
                sort_planets_by_distance(planets, length);
                break;
            }
            default: break;

        }

    }while(mode!=0);

    delete [] planets;
    return 0;
}

