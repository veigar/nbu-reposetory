#include <iostream>
#include <iomanip>
using namespace std;

int main() {
    const int N = 3;
    const int M = 4;
    int my_array[N][M] = { { 1, 2, 3, 4 }, { 4, 5, 6, 7 }, { 7, 8, 9, 10 } };
    int min_number = my_array[0][0], max_number = my_array[0][0];
    for( int i = 0; i < N; i++ ) {
        for( int j = 0; j < M; j++ ) {
            if( min_number > my_array[i][j] ) min_number = my_array[i][j];
            else if ( max_number < my_array[i][j] ) max_number = my_array[i][j];
            cout << setw(5) << my_array[i][j];
        }
        cout << endl;
    }
    cout << "The smallest number in the array is: " << min_number << endl;
    cout << "The largest number in the array is: " << max_number << endl;



    return 0;
}
