#include <iostream>
using namespace std;

int main() {

    int number;
    do{
        cout << "Please enter a number from 2 to 200" << endl;
        cin >> number;
    }while ( number <= 1 || number >= 90 );

    int temp = number;
    if( temp>=40 && temp<=50 ) {
        cout << "XL";
        temp-=40;
    } else {
        if( temp > 50 ) {
            cout << "L";
            temp -= 50;
        }
        switch(temp/10) {
            case 3: cout << "X";
            case 2: cout << "X";
            case 1: cout << "X";
        }
    }


    switch( temp%10 ) {
        case 1:
            cout << "I";
            break;
        case 2:
            cout << "II";
            break;
        case 3:
            cout << "III";
            break;
        case 4:
            cout << "IV";
            break;
        case 5:
            cout << "V";
            break;
        case 6:
            cout << "VI";
            break;
        case 7:
            cout << "VII";
            break;
        case 8:
            cout << "VIII";
            break;
        case 9:
            cout << "IX";
            break;
    }

    return 0;
}
