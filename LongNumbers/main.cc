#include <iostream>
#include <string>

using namespace std;

bool is_digits(const string &str)
{
    return str.find_first_not_of("0123456789") == string::npos;
}

void swap_digits( string &number_one, string &number_two ) {
    int length;
    char c;
    number_one.length() > number_two.length() ? length = number_one.length() : length = number_two.length();

    for( int i = 0; i < length; i++ ) {
        if( number_one[i] % 2 != 0 && number_two[i] % 2 == 0 ) {
           c = number_one[i];
           number_one[i] = number_two[i];
           number_two[i] = c;
        }

    }
}

int main() {
    string number_one;
    string number_two;
    do {
      cout << "Please enter a long number ( 30+ digits)" << endl;
      cin >> number_one;
    } while ( number_one.length() < 30 || !(is_digits(number_one)));
    do {
      cout << "Please enter a second long number ( 30+ digits)" << endl;
      cin >> number_two;
    } while ( number_two.length() < 30 || !(is_digits(number_two)));
    swap_digits(number_one, number_two);
    cout << number_one << endl;
    cout << number_two << endl;

    return 0;
}
