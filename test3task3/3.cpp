/***
FN:F73826
PID:3
GID:3
*/

#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <stdlib.h>

using namespace std;

void initialize_vector( vector<double> &v, int n ) {
    double value;
    for(int i = 0; i < n; i++ ) {
        cin >> value;
        v.push_back(value);
    }
}

void insert_to_vector( vector<double> &v, int position, double value ) { v.insert(v.begin()+position, value); }
void delete_from_vector( vector<double> &v, int position ) { v.erase(v.begin()+position); }
void update_range( vector<double> &v, int start_pos, int end_pos, double value ) {
    for( int i = start_pos; i <= end_pos; i++ ) {
        v[i] += value;
    }
}

void print_vector( vector<double> &v ) {
    for( int i=0; i < v.size(); i++ ) {
        cout << v[i] << " ";
    }
    cout << endl;
}

int main()
{
	int N, position, endposition, value;
	cin >> N;
	cin.ignore();
	vector<double> v;
	initialize_vector(v,N);
    string operation;
    while( cin >> operation ) {
        if( operation.compare("insert") == 0 ) {
            cin >> position;
            cin >> value;
            insert_to_vector( v, position, value );
        } else if ( operation.compare("delete") == 0) {
            cin >> position;
            delete_from_vector(v,position);
        } else if( operation.compare("update_range") == 0) {
            cin >> position;
            cin >> endposition;
            cin >> value;
            update_range(v, position, endposition, value);
        } else if( operation.compare("print") == 0 ) {
            print_vector(v);
        } else if( operation.compare("exit") == 0 ) {
            break;
        }
    }

	return 0;
}

