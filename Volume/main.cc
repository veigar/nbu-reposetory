#include <iostream>
#include <cmath>
using namespace std;
#define PI 3.14159265359

double sphereVolume( double r ) {
    static int cnt;
    cnt++;
    cout << "SphereVolume is called for " << cnt << " time." << endl;
    return (4*PI*pow(r, 3)) / 3;
}

int main() {
    double r;
    cout << "Please enter the sphere radius" << endl;
    cin >> r;
    cout << "Sphere with radius " << r << " has volume " << sphereVolume(r) << endl;
    cout << sphereVolume(3) << endl;

    return 0;
}
