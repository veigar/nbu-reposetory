#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;


int * findOcurranceArr( int number, int c, int &length, int &times) {


    length = 0;
    times = 0;
    int m = number;
    while( m > 0 ) {
        m /= 10;
        length++;
    }
    int *arr = new int[length];
    for(int i = 0; i < length; i++ ) {
        arr[i] = number%10;
        number/=10;
    }
    for( int i = 0; i < length; i++ ) {
        if(arr[i]==c) times++;
    }

    return arr;
}

void printArray( int * arr, int &length ) {
    for(int i = length-1; i >= 0; i--) {
        cout << arr[i] << setw(4);
    }
    cout << endl;
}
int main() {
    int number;
    int a;
    cout << "Enter number plox" << endl;
    cin >> number;
    cout << "Enter number to search for" << endl;
    cin >> a;
    int length, times;
    int *arr = findOcurranceArr(number, a, length, times);
    cout << "The digit " << a << " is found " << times << " in the number " << number << " which has " << length << " digits" << endl;
    printArray(arr, length);
    delete [] arr;


    return 0;
}
