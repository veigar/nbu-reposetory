/***
FN:F73826
PID:3
GID:3
*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;

void initialize_vector( vector<double> &v, int n ) {
    double value;
    for(int i = 0; i < n; i++ ) {
        cin >> value;
        v.push_back(value);
    }
}

void add_to_vector( vector<double> &v, int position, double value ) { v.insert(v.begin()+position-1, value); }
void remove_from_vector( vector<double> &v, int position ) { v.erase(v.begin()+position-1); }
void shift_left_vector( vector<double> &v ) {
    double temp = v[0];
    remove_from_vector( v, 0);
    v.push_back(temp);
}

void print_vector( vector<double> &v ) {
    for( int i=0; i < v.size(); i++ ) {
        cout << v[i] << " ";
    }
    cout << endl;
}

void reverse_vector( vector<double> &v ) {
    double temp;
    for( int i = 0; i < v.size()/2; i++ ) {
        temp = v[i];
        v[i] = v[v.size()-1-i];
        v[v.size()-1-i] = temp;
    }
}

int main()
{
	int N, position, value;
	cin >> N;
	cin.ignore();
	vector<double> v;
	initialize_vector(v,N);
    string operation;
    while( cin >> operation ) {
        if( operation.compare("add") == 0 ) {
            cin >> position;
            cin >> value;
            add_to_vector( v, position, value );
        } else if ( operation.compare("remove") == 0) {
            cin >> position;
            remove_from_vector(v,position);
        } else if( operation.compare("shift_left") == 0) {
            shift_left_vector(v);
        } else if( operation.compare("reverse") == 0 ) {
            reverse_vector(v);
        } else if( operation.compare("print") == 0 ) {
            print_vector(v);
        } else if( operation.compare("exit") == 0 ) {
            break;
        }
    }

	return 0;
}
