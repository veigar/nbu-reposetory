#include <string>
#include <iostream>

using namespace std;

string remove_upper_case( string s ) {
    string str;
    int i = 0;
    char c;
    while( s[i] ) {
        c = s[i];
        if( isupper(c) ) str+=c;
        i++;
    }
    return str;
}

int main() {
    int n;
    cin >> n;
    string *arr = new string[n];
    for( int i = 0; i < n; i++ ) {
        cin >> arr[i];
    }
    for( int i = 0; i < n; i++ ) {
        cout << remove_upper_case(arr[i]) << endl;
    }

    return 0;
}
