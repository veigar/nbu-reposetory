#include <iostream>
#include <iomanip>
using namespace std;
//Vsichki dvoiki bez povtoreniq
int main() {
    int range_begin;
    cout << "Please enter a number for the beginning of your range." << endl;
    cin >> range_begin;
    int range_end;
    cout << "Please enter a number for the end of your range" << endl;
    cin >> range_end;
    int counter = 0, last_count = 0, r, a,b;
    for( int i = range_begin; i <= range_end; i++ ) { // 1, 2
        for( int j = range_begin; j <= range_end; j++) {
            if( i != j ) {
                a = i;
                b = j;
                r = a % b;
                while( r !=0 ) {
                    a = b;
                    b = r;
                    r = a % b;
                    counter++;
                }
                if( last_count < counter ) last_count = counter;
                counter = 0;
            }
        }

    }
    cout << last_count << endl;


    return 0;
}
