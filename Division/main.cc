#include <iostream>
using namespace std;

int main () {
    int a, b ;
    cout << "Please input values for A and B" << endl;
    cin >> a >> b;
    if( b == 0 ) {
        cout << "You can't divide by 0!" << endl;
    } else {
        cout << "Result: " << a/b << endl;
    }

    return 0;
}

