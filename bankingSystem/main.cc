#include "bank.h"
#include <iostream>

using namespace std;


int main(){
	Bank b;
	Bank * bank = new Bank("123", "Ivan", 456.76);
	cout << "Deposit number: " << bank->get_deposit_number() << endl;
	cout << "Deposit owner: " << bank->get_deposit_owner() << endl;
	bank->show_balance();
	bank->add_to_balance(123);
	bank->show_balance();
	bank->remove_from_balance(300);
	bank->show_balance();
    cout << b.get_deposit_owner() << endl;
	delete bank;
	return 0;
}
