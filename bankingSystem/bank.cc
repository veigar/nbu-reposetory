#include "bank.h"
#include <iostream>
#include <string.h>
Bank::Bank(){
	_deposit_number = "No number";
	_deposit_owner = "No owner";
	_balance = 0;
}

Bank::Bank(char * number, char * owner, double balance){
	this->_deposit_number = new char[strlen(number)+1];
	strcpy(this->_deposit_number, number);
	this->_deposit_owner = new char[strlen(_deposit_owner)+1];
	strcpy(this->_deposit_owner, owner);
	this->_balance = balance;
}

Bank::~Bank(){
	delete [] _deposit_number;
	delete [] _deposit_owner;
}

void Bank::set_deposit_number(char * number){
	this->_deposit_number = new char[strlen(number)+1];
	strcpy(this->_deposit_number, number);
}

void Bank::set_deposit_owner(char * owner){
	this->_deposit_owner = new char[strlen(owner)+1];
	strcpy(this->_deposit_owner, owner);
}

void Bank::set_balance(double balance){
	this->_balance = balance;
}

char * Bank::get_deposit_number() const{
	return _deposit_number;
}

char * Bank::get_deposit_owner() const{
	return _deposit_owner;
}

double Bank::get_balance() const{
	return _balance;
}

void Bank::show_balance() const{
	std::cout << "Your balance is: " << _balance << std::endl;
}
void Bank::add_to_balance( double sum ) {
    this->_balance+=sum;
}
void Bank::remove_from_balance( double sum ) {
    this->_balance-=sum;
}

