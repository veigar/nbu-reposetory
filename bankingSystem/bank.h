#ifndef _BANK_H
#define _BANK_H

class Bank {
private:
	char * _deposit_number;
	char * _deposit_owner;
	double _balance;
public:
	Bank();
	Bank(char *, char *, double);
	~Bank();
	void set_deposit_number(char *);
	void set_deposit_owner(char *);
	void set_balance(double);
	char * get_deposit_number() const;
	char * get_deposit_owner() const;
	double get_balance() const;
	void show_balance() const;
    void add_to_balance( double sum);
    void remove_from_balance( double sum );

};

#endif
