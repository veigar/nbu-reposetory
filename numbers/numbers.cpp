#include <iostream>

using namespace std;

int main() {
    int number;
    do{
        cout << "Please enter a number from 2 to 200" << endl;
        cin >> number;
    }while ( number < 2 || number >= 200 );

    double sum = 1;
    double k = 1;
    cout << sum;
    for( int i = 2; i <= number; i++ ) {
        k = -k;
        sum+=k/i;

        ( k<0 ) ? cout << " + " : cout << " - ";
        cout << "1/" << i;
    }

    cout << " = " << sum << endl;

    return 0;
}
