#include <string>
#include <stack>
#include <iostream>
#include <ctype.h> // required for tolower()

using namespace std;

char* intToRoman(int num) {

    int value[]={1000,900,500,400,100,90,50,40,10,9,5,4,1},size,i,count;

    char rom[][3]={"M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"};

    static char roman[10];  //only needed to print in the caller function, hence static

    size=sizeof(value)/sizeof(value[0]);



    if(num>0 && num<4000) {

        for(i=0;i<size;i++) {

            count=num/value;

            num-=count*value;

            while(count) {

                strcat(roman,rom);

                --count;

            }
        }
    }
    return roman;
}


int main()
{

    int num;
    while( cin >> num ) {
        cout << intToRoman(num) << endl;
    }

    return 0;
}

