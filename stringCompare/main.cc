#include <iostream>
#include <string>
using namespace std;

bool stringCompare( string &first, string &second ) {
    bool equal = false;
    for( int i=0; i < first.length(); i++ ) {
        if( first.length() == second.length() ) {
            if( tolower(first[i]) == tolower(second[i]) ) equal = true;
            else equal = false;
        }
    }
    return equal;
}

int main() {
    string first,second;
    while(cin) {
        getline(cin, first);
        getline(cin, second);
        stringCompare(first,second) ? cout << "Yes" << endl : cout << "No" << endl;
    }

    return 0;
}
