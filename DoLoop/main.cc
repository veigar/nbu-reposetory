#include <iostream>
using namespace std;

int main() {
    int a, b, r;
    cout << "Please enter numbers to process:" << endl;
    cin >> a >> b;
    if ( b == 0 ) {
        cout << "Second number cant be 0." << endl;
        return -1;
    }
    do{
        r = a%b;
        a = b;
        b = r;
    }while ( r != 0);
    cout << a << endl;

}

