#include <iostream>
#include <cmath>
using namespace std;


int calculate_progression_sum(int k, int m, int n){
    int sum = 0;
    for( int i = 1; i <= n; i++ ) {
        sum+=pow(i,3)+pow(i,2)+k*m;
        cout << "Your current sum is: " << sum << endl;
    }
    return sum;
}

int main() {

    int k,m,n;
    cout << "Please enter values for K, M and N" << endl;
    cin >> k >> m >> n;
    calculate_progression_sum(k,m,n);
    return 0;
}
