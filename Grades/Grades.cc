#include <iostream>
using namespace std;

int main() {

    int first_grade, second_grade;
    cout << "Please enter the first grade: ";
    cin >> first_grade;
    if( first_grade < 2 || first_grade > 6 ) {
        cout << "This grade is not valid in Bulgaria" << endl;
        return -1;
    }
    cout << "Please enter the second grade: ";
    cin >> second_grade;
    if( second_grade < 2 || second_grade > 6 ) {
        cout << "This grade is not valid in Bulgaria" << endl;
        return -1;
    }

    cout << "Your average grade is: " << ((double)(first_grade+second_grade))/2 << endl;

    return 0;
}
