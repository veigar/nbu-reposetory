#include "student.hh"
#include <iostream>
#include <iomanip>


void add_student( Student & s){
    std::cout << "Enter students\'s faculty number: ";
    std::cin >> s.fnum;
    std::cout << "Enter students\'s name: ";
    std::cin.ignore();
    std::getline(std::cin, s.name);
    std::cout << "Enter students\'s grade: ";
    std::cin >> s.grade;
}
void print_student_data( Student const s ){
    std::cout << "Faculty number: " << s.fnum;
    std::cout << "\tName: " << s.name;
    std::cout << "\tGrade: " << s.grade;
}
void addStudents( Student * arrStudents, unsigned length){
    for( int i = 0; i < length; i++ ) {
        add_student(arrStudents[i]);
    }
}
void print_students_data( Student * arrStudents const, unsigned length ){
    for( int i = 0; i < length; i++ ) {
        print_student_data(arrStudents[i]);
    }
}
void sort_excelent_poor( Student * arrStudents, unsigned length ){
    int flag = 1;    // set flag to 1 to start first pass
    Student temp;             // holding variable
    for(unsigned i = 1; (i <= length) && flag; i++) {
        flag = 0;
        for (unsigned j=0; j < (length -1); j++) {
            if (arrStudents[j+1].grade > arrStudents[j].grade) {
                temp = arrStudents[j];             // swap elements
                arrStudents[j] = arrStudents[j+1];
                arrStudents[j+1] = temp;
                flag = 1;               // indicates that a swap occurred.
            }
        }
    }
}
void sort_poor_excelent( student * arrStudents, unsigned length ){
    int flag = 1;    // set flag to 1 to start first pass
    Student temp;             // holding variable
    for(unsigned i = 1; (i <= length) && flag; i++) {
        flag = 0;
        for (unsigned j=0; j < (length -1); j++) {
            if (arrStudents[j+1].grade < arrStudents[j].grade) {
                temp = arrStudents[j];             // swap elements
                arrStudents[j] = arrStudents[j+1];
                arrStudents[j+1] = temp;
                flag = 1;               // indicates that a swap occurred.
            }
        }
    }
}

