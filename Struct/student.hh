#ifdef _STUDENT_H
#define _STUDENT_H

#include <string>

struct Student{
    std::string fnum;
    std::string name;
    double grade;
};

void add_student( Student & );
void print_student_data( Student const );
void addStudents( Student *, unsigned );
void print_students_data( Student *, unsigned );
void sort_excelent_poor( Student *, unsigned );
void sort_poor_excelent( student *, unsigned );

#endif // _STUDENT_H
