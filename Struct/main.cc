
#include <iostream>

using namespace std;


int main() {
    const unsigned n = 3;
    Student students[3];
    addStudents( students, n );
    cout << "Student\'s list: " << endl;
    print_students_data( students, n );
    sort_excelent_poor( students, n );
    cout << "Sorted Student\'s list increase" << endl;
    print_student_data( students, n );
    sort_poor_excelent( students, n );
    cout << "Sorted Student\'s list decrease" << endl;
    print_student_data( students, n );

    return 0;
}
