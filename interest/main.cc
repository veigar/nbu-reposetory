#include <iostream>
#include <cmath>

using namespace std;

double simple_interest( double money, double interest, int years ) {
    double v = money*(interest/100);
    return money+v*years;
}

double compound_interest( double money, double interest, int years ) {
    double v = ( 1 + interest/100 );
    return money*pow(v, years);
}

int main() {
    double money, interest;
    int years;

    while( cin >> money >> interest >> years ) {
        cout << simple_interest( money, interest, years ) << endl;
        cout << compound_interest( money , interest, years ) << endl;
    }

    return 0;
}
