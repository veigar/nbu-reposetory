#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;

vector<int> primes;

void isPrime(int n)
{
    vector<int>::iterator it;
    for(int ind=2;ind<=n;ind++)
    {
        if (n%ind==0) {
            it = find( primes.begin(), primes.end(), sqrt(n) );
            if( it == primes.end() ) {
                cout << n << " ";
                primes.insert( primes.end(), sqrt(n) );
            }
        }

    }
}


int main() {
    int number;
    vector<int>::iterator it;

    cout << "Please enter a number to factorize" << endl;
    cin >> number;

    for( int i = 2; i <= number; i++ ) {
        it = find( primes.begin(), primes.end(), sqrt(number) );
        if ( it != primes.end() ){

            while (  number%i == 0 ) {
              cout << i << " ";
              number = number / i;
            }
        } else {
            isPrime(sqrt(number));
        }
    }

    for( int i = 0; i < primes.size(); i++ ) {
        cout << primes[i] << " ";
    }
    cout << endl;


    return 0;
}
