#include "tasks.h"
#include <stdlib.h>
#include <cmath>
#include <iostream>

Animal::Animal() {
    name = "No name";
    age = 0;
    weight = 0;
}

Animal::Animal(char * n, int a, double w ) {
    name = new char[sizeof(n)+1];
	if(name!= NULL ) name = n;
    age = a;
    weight = w;
}
Animal::~Animal() {
    delete [] name;
}
void Animal::set_name(char * n ) {
	name = new char[sizeof(n)+1];
	if(name!= NULL ) name = n;
}
void Animal::set_age(int a){
	age = a;
}
void Animal::set_weight(double w){
	weight = w;
}
char * Animal::get_name() const{
	return name;
}
int Animal::get_age() const{
	return age;
}
double Animal::get_weight() const{
	return weight;
}
void Animal::set_animal_data() {
    name = new char [256];
	std::cout << "Enter Animal name: ";
	std::cin.getline(name, 255);
	std::cout << "Enter Animal age: ";
	std::cin >> age;
	std::cout << "Enter Animal weight: ";
	std::cin >> weight;
}

void Animal::print_animal_data() {
    std::cout << "Animal name: " << name << std::endl
			  << "Animal age: " << age << std::endl
			  << "Animal weight: " << weight << std::endl;
}


void fillArray( int ** arr, unsigned n, unsigned m ) {
    for(unsigned i=0; i<n; i++)
		for(unsigned j=0; j<m; j++)
			arr[i][j] = rand()%20-5;
}
void printArray( int ** arr, unsigned n, unsigned m ) {
    for(unsigned i=0; i<n; i++) {
        for( unsigned j=0; j<m; j++) {
            std::cout << arr[i][j] << "\t";
        }
        std::cout << std::endl;
    }
}
void manipulateArray( int ** arr, unsigned n, unsigned m ) {
	for(unsigned i=0; i<n; i++){
        for( unsigned j=0; j<m; j++ ) {
            if( j>i ) { arr[i][j] += (i+j)*(i+j); }
            else { arr[i][j] = 0; }
        }
    }
}
