#ifndef _TASKS_H
#define _TASKS_H

class Animal {
    private:
        char * name;
        int age;
        double weight;
    public:
        Animal();
        Animal(char *, int, double);
        ~Animal();
        void set_name(char *);
        void set_age(int);
        void set_weight(double);
        char * get_name() const;
        int get_age() const;
        double get_weight() const;
        void set_animal_data();
        void print_animal_data();

};


void fillArray(int **, unsigned, unsigned);
void printArray(int **, unsigned, unsigned);
void manipulateArray( int **, unsigned, unsigned);


#endif
