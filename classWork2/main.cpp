#include <iostream>
#include <stdlib.h>
#include <ctime>
#include "tasks.h"
using namespace std;

int main() {


    const unsigned N = 5;
    const unsigned M = 5;
    srand(time(NULL));

    int ** arr = new int *[N];
    for(int i = 0; i < N; ++i)
		arr[i] = new int[M];

    fillArray(arr, N, M);
	printArray(arr, N, M);
	cout << endl;
	manipulateArray(arr, N, M);
	printArray(arr, N, M);

    Animal a;
    Animal cat = Animal("Trinity", 2, 5);
    a.set_animal_data();
    a.print_animal_data();
    cat.print_animal_data();


    return 0;
}
