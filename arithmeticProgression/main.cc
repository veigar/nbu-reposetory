#include <iostream>
using namespace std;

double calculateProgression( int start, double d, int length ) {
    double sum = 0;
    for( int i = 0; i < length; i++) {
        cout << "element: " << i << " = " << start + i*d << endl;
        sum += start+i*d;
    }
    return sum;
}

int main() {

    int start, length;
    double d;

    do{
        cout << "Please enter a starting value for the progression: ";
        cin >> start;
    } while( start < 1.1 || start > 99.9 );


    do {
        cout << "Please enter the difference between the progression members: ";
        cin >> d;
    } while( d < 1.1 || d > 99.9 );

    cout << "Please enter the length of the progression: ";
    cin >> length;
    double s = calculateProgression( start, d, length );

    cout << "The sum of the progression is " << s << endl;

    return 0;
}
