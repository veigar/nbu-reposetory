#include <iostream>
#include <cmath>
using namespace std;


long int numberPow( unsigned int M, int &length ){
    long int number = 0;
    for( int i = 0; i < M; i++ ) {
        number = number*10 + 1;
    }
    number = number*number;
    long int m = number;
    length = 0;
    while( m > 0 ) {
        m/=10;
        length++;
    }
    return number;
}

int main() {

    unsigned int M;
    cout << "Please enter the power of M" << endl;
    cin >> M;
    int length;
    cout << "There number constructed by " << M << " 1s on power of two is " << numberPow(M, length);
    cout << " and has " << length << " digits." << endl;


    return 0;
}
