#include <iostream>
#include <sstream>
#include <math.h>
using namespace std;


int main() {
    int X, Y;
    cout << "Please enter X coordinates" << endl;
    cin >> X;
    cout << "Please enter Y coordinates" << endl;
    cin >> Y;
    if ( X == 0 && Y == 0) {
        cout << "Your point is in the center." << endl;
    } else if( X >= 0 && Y >= 0 ) {
        cout << "Your point is in I quadrant." << endl;
    } else if ( X <= 0 && Y >= 0 ) {
        cout << "Your point is in II quadrant." << endl;
    } else if ( X <= 0 && Y <= 0 ) {
        cout << "Your point is in III quadrant." << endl;
    } else if ( X >= 0 && Y <= 0 ) {
        cout << "Your point is in IV quadrant." << endl;
    }

    cout << "Please enter X coordinates" << endl;
    cin >> X;
    cout << "Please enter Y coordinates" << endl;
    cin >> Y;

    int quadrant;
    ( X == 0 && Y == 0 ) ? quadrant = 0 :
        ( X >= 0 && Y >= 0 ) ? quadrant = 1 :
            ( X <= 0 && Y >= 0 ) ? quadrant = 2 :
                ( X <= 0 && Y <=0 ) ? quadrant = 3 :
                    ( X >= 0 && Y <=0 ) ? quadrant = 4 : quadrant = -1;
    switch( quadrant ) {
    case 0:
        cout << "Your point is in the center." << endl;
        break;
    case 1:
        cout << "Your point is in I quadrant." << endl;
        break;
    case 2:
        cout << "Your point is in II quadrant." << endl;
        break;
    case 3:
        cout << "Your point is in III quadrant." << endl;
        break;
    case 4:
        cout << "Your point is in IV quadrant." << endl;
        break;
    default:
        cout << "Your point is invalid." << endl;
        break;
    }

    struct Point {
     int X;
     int Y;
    };

    int array_length;
    cout << "Please enter the amount of points you wish to check" << endl;
    cin >> array_length;
    struct Point points[array_length];
    int closest_distance = 0, current_point_distance = 0, closest_point_index;
    for( int i = 0; i < array_length; i++ ) {
        cout << "Please enter X coordinates" << endl;
        cin >> points[i].X;
        cout << "Please enter Y coordinates" << endl;
        cin >> points[i].Y;
    }
    for( int i = 0; i < array_length; i++ ) {
        if ( points[i].X == 0 && points[i].Y == 0) {
            cout << "Your point is in the center." << endl;
        } else if( points[i].X >= 0 && points[i].Y >= 0 ) {
            cout << "Your point is in I quadrant." << endl;
        } else if ( points[i].X <= 0 && points[i].Y >= 0 ) {
            cout << "Your point is in II quadrant." << endl;
        } else if ( points[i].X <= 0 && points[i].Y <= 0 ) {
            cout << "Your point is in III quadrant." << endl;
        } else if ( points[i].X >= 0 && points[i].Y <= 0 ) {
            cout << "Your point is in IV quadrant." << endl;
        }


        current_point_distance = sqrt( (double) pow( points[i].X  , 2 ) + pow( points[i].Y , 2 ) );
        if( i == 0 ) {
            closest_distance = current_point_distance;
            closest_point_index = i;
        } else if( current_point_distance < closest_distance ){
            closest_distance = current_point_distance;
            closest_point_index = i;
        }
        cout << closest_point_index << endl;
    }
    cout << "The point closest to the center is: " << points[closest_point_index].X <<  " " << points[closest_point_index].Y << endl;

    return 0;
}
