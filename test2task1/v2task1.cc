#include <iostream>

using namespace std;

int main() {

    int n;
    cin >> n;
    double mMin;
    double mNumber;
    for( int i = 0; i < n; i++ ) {
        cin >> mNumber;
        if( i == 0 ) mMin = mNumber;
        else if( mNumber < mMin ) mMin = mNumber;
    }
    cout << mMin << endl;

    return 0;
}
