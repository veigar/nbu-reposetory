#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

void modifyData(string &fn, char g ) {
    double grade;
    switch( g ) {
    case 'F':
        grade = 2;
        break;
    case 'E':
        grade = 3;
        break;
    case 'C':
        grade = 4;
        break;
    case 'B':
        grade = 5;
        break;
    case 'A':
        grade = 6;
        break;
    default:
        grade = 0;
        break;

    }
    cout << fn.append("\t") << fixed << setprecision(2) << grade << endl;
}

int main() {
    string fn;
    char g;
    cout << "Enter faculty numbers and grades: " << endl;
    while (cin >> fn >> g)
	{
		modifyData( fn , g );
	}

}
