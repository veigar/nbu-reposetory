/***
FN:F73826
PID:2
GID:3
*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;


void devour_vector( vector<double> &v1, vector<double> &v2 ) {
    for( int i = 0; i < v2.size(); i++ ) {
        v1.push_back(v2[i]);
    }
}

void print_vector( vector<double> &v ) {
    for( int i=0; i < v.size(); i++ ) {
        cout << v[i] << " ";
    }
    cout << endl;
}

void initialize_vector( vector<double> &v, int n ) {
    double value;
    for(int i = 0; i < n; i++ ) {
        cin >> value;
        v.push_back(value);
    }
}


int main()
{
	int N, M;
	cin >> N;
	cin.ignore();
	vector<double> v1;
	initialize_vector(v1,N);
    cin >> M;
    cin.ignore();
	vector<double> v2;

    initialize_vector(v2,M);
    devour_vector(v1, v2);
    print_vector(v1);

	return 0;
}
