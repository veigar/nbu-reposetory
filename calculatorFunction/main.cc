#include <iostream>
using namespace std;


double substract( double left_operand, double right_operand ) {
    return left_operand-right_operand;
}

double multiply( double left_operand, double right_operand ) {
    return left_operand*right_operand;
}

double devide( double left_operand, double right_operand ) {
    if( right_operand != 0 ) {
        return left_operand/right_operand;
    }
}

double add( double left_operand, double right_operand ) {
    return left_operand+right_operand;
}

double do_operation(double left_operand, double right_operand, char operation) {

    switch(operation) {
    case '-':
        return substract( left_operand, right_operand );
    case '+':
        return add( left_operand, right_operand );
    case '*':
        return multiply( left_operand, right_operand );
    case '/':
        return devide( left_operand, right_operand );
    }
}

int main() {

    double left_operand, right_operand;
    char operation;
    while( cin >> left_operand >> operation >> right_operand ) {
        int res = do_operation(left_operand, right_operand, operation);

        cout << res << endl;
    }

    return 0;
}
