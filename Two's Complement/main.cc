#include <iostream>
#include <iomanip>
using namespace std;

int main() {
    const int length = 4;
    int a[length] = { 0, 1, 2, 3};
    int b[length] = { 4, 2, 1 ,5 };
    int c[length+1];
    int sum;
    int remain = 0;

    for ( int i = length-1; i >= 0; i-- ) {
        sum = a[i]+b[i] + remain;
        c[i+1] = sum % 10;
        remain = sum / 10;
    }
    c[0] = remain;
    for ( int i = 0; i < length+1; i++ ) {
        cout << c[i];
    }



    return 0;
}
