#include <iostream>
#include <cmath>
using namespace std;



int isPrime(int num,int i){

    if(i==1){
        return 1;
    }else{
       if(num%i==0)
         return 0;
       else
         isPrime(num,i-1);
    }
}


int main() {
    int number;
    cout << "Enter a number to evaluate." << endl;
    cin >> number;
    cout << isPrime( number, number/2) << endl;

    return 0;
}
