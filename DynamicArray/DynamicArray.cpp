#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int *dynamicArray;

    int rows, columns;

    do{
		cout<<"Please enter the rows of your dynamicArrayay: ";
		cin>>rows;
	}while(rows<=0 && rows>=9);

	do{
		cout<<"Please enter the columns of your dynamicArrayay: ";
		cin>>columns;
	}while(columns<=0 && columns>=9);

    dynamicArray= new int[rows*columns];

    for(int i=0;i<rows*columns;i++){
		cout<<"Enter a value for the element # " << i << ": ";
		cin>>dynamicArray[i];
	}

	for( int i = 0; i < rows*columns; i++ ) {

        cout << setw(5) << dynamicArray[i] ;

        if( ( i + 1 ) % columns == 0 ) {
            cout << endl;
        }
    }
    cout << endl;

    int row_one, row_two;
    bool invalid;
    do{
		cout<<"Please enter the first rows you wish to swap: " << endl;
		cin>>row_one;
        cout<<"Please enter the second rows you wish to swap: " << endl;
		cin>>row_two;
		if( row_one < 0 || row_one >= columns || row_two <0 || row_two >= columns ) {
            cout << "Please enter valid rows to swap" << endl;
            invalid = true;
		} else {
            invalid = false;
		}
	}while( invalid );

    for ( int i=0; i < columns; i++) {
        int temp = *((int *)dynamicArray + columns*row_one + i );
        *((int *)dynamicArray + columns*row_one + i ) = *((int *)dynamicArray + columns*row_two + i );
        *((int *)dynamicArray + columns*row_two + i ) = temp;
    }

    for( int i = 0; i < rows*columns; i++ ) {

        cout << setw(5) << dynamicArray[i] ;

        if( ( i + 1 ) % columns == 0 ) {
            cout << endl;
        }
    }
    cout << endl;

    int column_one, column_two;
    do{
		cout<<"Please enter the first column you wish to swap: " << endl;
		cin>>column_one;
        cout<<"Please enter the second column you wish to swap: " << endl;
		cin>>column_two;

		if( column_one<0 || column_one>=columns || column_two <0 || column_two >= columns ) {
            cout << "Please enter valid columns to swap" << endl;
            invalid = true;
		} else {
            invalid = false;
		}
	}while( invalid );

    for ( int i=0; i < rows; i++) {
        int temp = *((int *)dynamicArray + i*columns + column_one );
        *((int *)dynamicArray + i*columns + column_one ) = *((int *)dynamicArray + i*columns + column_two ) ;
        *((int *)dynamicArray + i*columns + column_two ) = temp;
    }

    for( int i = 0; i < rows*columns; i++ ) {

        cout << setw(5) << dynamicArray[i] ;

        if( ( i + 1 ) % columns == 0 ) {
            cout << endl;
        }
    }
    cout << endl;


    delete []dynamicArray;
    return 0;
}
