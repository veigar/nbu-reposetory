#include <iostream>
using namespace std;

void printLetterTriangle( char c ) {
    int finalLetter = int(c);
    for( int i = 'A'; i <= finalLetter; i++ ) {
        for( int j = 'A'; j < i; j++ ) {
            cout << char(j);
        }
        cout << endl;
    }
    for( int i = 'A'; i <= finalLetter; i++ ) {
        cout << char(i);
    }
    cout << endl;
}

int main() {
    char c;
    cout << "Enter the final letter of your letter triangle ! :D" << endl;
    cin >> c;
    printLetterTriangle(c);

    return 0;
}
