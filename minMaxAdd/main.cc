#include <iostream>
using namespace std;

#define MAGIC_NUMBER 15
int main () {
    int a, b ;
    cout << "Please input values for A and B" << endl;
    cin >> a >> b;
    if( a < b ) {
        cout << "Result: " << a + MAGIC_NUMBER << endl;
    } else {
        cout << "Result: " << b + MAGIC_NUMBER << endl;
    }

    return 0;
}
