#include <iostream>
#include <iomanip>
using namespace std;

int main() {
    const int N = 3;
    const int M = 4;
    int number;
    int my_array[N][M];
    for( int i = 0; i < N*M; i++ ) {
        cout << "Please enter value for the " << i << " element of the array: ";
        cin >> number;
        if( !cin.fail() ) {
            *((int *)my_array + i) = number;
        }
    }
    for( int i = 0; i < N; i++ ) {
        for( int j = 0; j < M; j++ ) {
            if( i%2 == 0 && j%2 == 0 ) {
                cout << "[" << my_array[i][j] << "]";
            }  else if ( i%2 == 1 && j%2 == 1 ) {
                cout << "[" << my_array[i][j] << "]";
            }  else {
                cout << "[ ]";
            }
        }
        cout << endl;
    }



    return 0;
}

