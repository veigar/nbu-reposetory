#include <iostream>
#include <iomanip>
using namespace std;

int main () {
    int width = 3;
    int height = 4;
    int arr[3][4] =  { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };
    for( int i = 0; i < width; i++ ) {
        for( int k = 0; k < height; k++ ) {
            cout << setw(5) << arr[i][k] ;
        }
        cout << endl;
    }
    cout << endl;

    for ( int i=0; i < height; i++) {
        int temp = arr[0][i];
        arr[0][i] = arr[2][i];
        arr[2][i] = temp;
    }

    for( int i = 0; i < width; i++ ) {
            for( int k = 0; k < height; k++ ) {
                cout << setw(5) << arr[i][k] ;
            }
            cout << endl;
        }
    cout << endl;

    for ( int i=0; i < width; i++) {
        int temp = arr[i][1];
        arr[i][1] = arr[i][3];
        arr[i][3] = temp;

    }
    for( int i = 0; i < width; i++ ) {
            for( int k = 0; k < height; k++ ) {
                cout << setw(5) << arr[i][k] ;
            }
            cout << endl;
        }
    cout << endl;

    return 0;
}
