#include <iostream>
#include <string>

using namespace std;

const double PI = 3.14;
const int REFLEX_ANGLE = 180;

int main() {
    double angle;
    char sign;
    double result;

    cout << "Please enter the angles value." << endl;
    cin >> angle;
    cout << "Please enter the R for radians or D for degrees" << endl;
    cin >> sign;

    if( sign == 'D' ) {
        result = angle * ( PI / REFLEX_ANGLE );
        cout << "Your result in radians is: " << result << endl;
    } else if ( sign =='R' ) {
        result = angle * ( REFLEX_ANGLE / PI );
        cout << "Your result in degrees is: " << result << endl;
    } else {
        cout << "Please enter a valid sign" << endl;
        return -1;
    }

    return 0;
}
