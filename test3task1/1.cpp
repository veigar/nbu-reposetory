/***
FN:F73826
PID:1
GID:2
*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;

double happy_vectors( vector<double> &v1, vector<double> &v2 ) {
    double sum = 0;
    for( int i = 1; i < v1.size(); i+=2 ) {
           ( i == 1 ) ? sum += v1[i]*v2[i] : sum -= ( v1[i]*v2[i] );
    }
    return sum;
}



void initialize_vector( vector<double> &v, int n ) {
    double value;
    for(int i = 0; i < n; i++ ) {
        cin >> value;
        v.push_back(value);
    }
}


int main()
{
	int N;
	cin >> N;
	cin.ignore();
	vector<double> v1;
	vector<double> v2;
	initialize_vector(v1,N);
    initialize_vector(v2,N);
    cout << happy_vectors(v1, v2);
	return 0;
}
