#include <iostream>
using namespace std;

int main() {
    int n;
    int sum = 0;
    while( cin >> n ) {
        if( n%2 == 0 && n%3 != 0 ) sum+=n;
    }
    cout << "Your sum is: " << sum;

    return 0;
}
