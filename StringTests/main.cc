#include <cstdlib>
#include <iostream>
#include <ctime>
#include <iomanip>
#include "functions.h"
using namespace std;

int main() {

    srand( (unsigned int) time(NULL));

    string s;
    cin >> s;
    cout << boolalpha << is_palindrome( s ) << endl;
    cout << boolalpha << is_same_symbol( s ) << endl;
    remove_vowels( s );
    cout << s << endl;
    cout << generate_random_string() << endl;

    return 0;
}

