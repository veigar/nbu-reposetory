#include "functions.h"
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <string>

char generate_random_consonant() {

    char consonant = 'a' + rand() % 26;

    do{
        consonant = 'a' + rand() % 26;
    }while (consonant == 'a' || consonant == 'e' || consonant == 'i' || consonant == 'o' || consonant == 'u' || consonant == 'y');

    return consonant;
}

char generate_random_symbol() {
    return ' ' + rand() % 95;
}

bool is_palindrome( std::string s ) {

    return s == std::string(s.rbegin(), s.rend());
}

bool is_same_symbol( std::string s ) {
    char c;
    std::cout << "Enter a character to compare" << std::endl;
    std::cin >> c;
    return s[s.length()/2] == c;
}

void remove_vowels( std::string & s ){
    for( int i=0; i < s.length(); i++ ) {
        if(s[i] == 'a' || s[i] == 'e' || s[i] == 'i' || s[i] == 'o' || s[i] == 'u' || s[i] == 'y') {
            s[i] = generate_random_consonant();
        }
    }
}

std::string generate_random_string() {

    const int length = 1 + rand() % 10;
    std::string s = "";
    for( int i = 0; i < length; i++ ) {
        s.append(1, generate_random_symbol());
    }
    return s;
}
