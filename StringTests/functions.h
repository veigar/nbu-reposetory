#ifndef _FUNCTIONS_H
#define _FUNCTIONS_H

#include <string>

char generate_random_consonant();
char generate_random_symbol();
bool is_palindrome( std::string );
bool is_same_symbol( std::string );
void remove_vowels( std::string & );
std::string generate_random_string();

#endif // _FUNCTIONS_H
