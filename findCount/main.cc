#include <iostream>
#include <iomanip>
using namespace std;


int * findCnt(int n, int c, int &length, int &cnt){
    length = 0;
    cnt = 0;
    int m = n;
    while( m > 0 ) {
        m /= 10;
        length++;
    }
    int * arr = new int[length];
    for ( int i = 0; i < length; i++ ) {
        arr[i] = n%10;
		if(n%10 == c) cnt++;
		n = n/10;
    }

    return arr;
}

int main() {
    int n,c;
    cout << "Enter a number to evaluate." << endl;
    cin >> n;
    cout << "Enter a number to find." << endl;
    cin >> c;

    int length, cnt;
    int * arr = findCnt(n, c, length, cnt);
    cout << "The number of occurrence of " << c << " is " << cnt << endl;
    cout << "The array made from " << n << " looks like: " << endl;
    for( int i = length-1; i >= 0; i-- ) {
        cout << arr[i] << setw(5);
    }
    cout << "\nWith length = " << length;

    return 0;
}
